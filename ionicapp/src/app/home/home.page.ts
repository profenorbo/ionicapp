import { Component, OnInit /* Paso 6 */} from '@angular/core';
import { DataService } from './../data.service'; //Paso 7
import { Observable } from 'rxjs'; //Paso 8

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  results: Observable<any>; //Paso 9
  constructor(private dataService: DataService /* Paso 10 */) {}
  ngOnInit() { this.getData();} //Paso 11
  //Paso 12
  getData() {
    // Call our service function which returns an Observable
    this.results = this.dataService.getData();
  }

}
