import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; //Paso 3
import { HttpClient } from '@angular/common/http'; //Paso 3
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) { } //Paso 4
  //Paso 5
  getData(): Observable<any> {
    return this.http.get('http://innobyte.mx/data2.json').pipe(map(results => results['personas']));
  }
}
